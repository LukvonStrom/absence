/* eslint-disable no-param-reassign */
const burns = require('burns');
const { reportHandler } = require('../../util');
const User = require('mongoose').model('User');
const crypto = require('crypto');

module.exports = {
  requestPassReset: (req, res) => {
    const { email } = req.body;
    User.findOne({ email }).exec().then((user) => {
      const randomString = crypto.randomBytes(64).toString('hex').substring(0, 12);
      burns.dispatch('dataArchiveRequest', {
        resetLink: `${process.env.APP_ROOT_URL}resetpw/${randomString}`, 
        user,
      });
      user.password_reclaim_hash = randomString;
      user.save((err) => {
        if (err) return reportHandler({ reporter: res.json, err });
      });
      reportHandler({ reporter: res.json, optionalValue: { details: 'Kicked off Delivery' } });
    });
  },
  finishPassReset: (req, res) => {
    const { pwRcHash, newPass } = req.body;
    User.findOne({ password_reclaim_hash: pwRcHash }).exec()
      .then((user) => {
        user.password = newPass;
        user.save((err) => {
          if (err) return reportHandler({ reporter: res.json, err });
        });
      })
      .catch(err => reportHandler({ reporter: res.json, err, optionalValue: { details: 'No User with this hash' } }));
  },
};

