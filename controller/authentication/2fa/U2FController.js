/* eslint-disable max-len,no-param-reassign */
const u2f = require('node-u2f');
const User = require('mongoose').model('User');
const { reportHandler } = require('../../../util');

function saveUser(user, res) {
  user.save((err) => {
    if (err) return reportHandler({ reporter: res.json, err });
  });
}
// GET
const firstRegisterStage = (req, res) => {
  if (!process.env.TFA_ENABLED) { return reportHandler({ reporter: res.json, err: new Error('2FA globally deactivated') }); }
  const registerRequest = u2f.startRegistration(process.env.APP_ROOT_URL);
  User.temporaryStorage.u2f_registerRequest = registerRequest;
  saveUser(User, res);
  reportHandler({ reporter: res.json, optionalValue: registerRequest });
};
// Post
const secondRegisterStage = (req, res) => {
  if (!process.env.TFA_ENABLED) { return reportHandler({ reporter: res.json, err: new Error('2FA globally deactivated') }); }
  const { RegisterResponse } = req.body;
  User.findOne({ email: req.user.email }).then((user) => {
    const deviceRegistration = u2f.finishRegistration(user.temporaryStorage.u2f_registerRequest, RegisterResponse);
    user.mfaStorage.deviceRegistration = deviceRegistration;
    user.u2f_enabled = true;
    saveUser(user, res);
    return reportHandler({ reporter: res.json, optionalValue: { success: true } });
  }).catch(err => reportHandler({ reporter: res.json, err }));
};

// GET
const firstLoginStage = (req, res) => {
  if (!process.env.TFA_ENABLED) { return reportHandler({ reporter: res.json, err: new Error('2FA globally deactivated') }); }
  User.findOne({ email: req.user.email }).then((user) => {
    const signRequest = u2f.startAuthentication(process.env.APP_ROOT_URL, user.mfaStorage.deviceRegistration);
    user.temporaryStorage.u2f_signRequest = signRequest;
    saveUser(user, res);
    reportHandler({ reporter: res.json, optionalValue: signRequest });
  }).catch(err => reportHandler({ reporter: res.json, err }));
};

// POST
const secondLoginStage = (req, res) => {
  if (!process.env.TFA_ENABLED) { return reportHandler({ reporter: res.json, err: new Error('2FA globally deactivated') }); }
  const signResponse = req.body.SignResponse;
  User.findOne({ email: req.user.email }).then((user) => {
    const deviceAuthentication = u2f.finishAuthentication(user.temporaryStorage.u2f_signRequest, signResponse, user.mfaStorage.deviceRegistration);
    user.mfaStorage.deviceAuthentication = deviceAuthentication;
    saveUser(user, res);
    reportHandler({ reporter: res.json, optionalValue: { success: true } });
  }).catch(err => reportHandler({ reporter: res.json, err }));
};

// TODO: Implement this with permissionscheck
const resetU2F = (req, res) => {
  if (!process.env.TFA_ENABLED) { return reportHandler({ reporter: res.json, err: new Error('2FA globally deactivated') }); }
  const { email } = req.body;
  User.findOne({ email }).then((user) => {
    user.temporaryStorage.u2f_signRequest = null;
    user.temporaryStorage.u2f_registerRequest = null;
    user.mfaStorage.deviceRegistration = null;
    user.mfaStorage.deviceAuthentication = null;
    user.u2f_enabled = false;
    saveUser(user, res);
    reportHandler({ reporter: res.json, optionalValue: { success: true } });
  }).catch(err => reportHandler({ reporter: res.json, err }));
};

const getU2FState = async (user, res) => {
  try {
    const target = await User.findOne({ email: user.email });
    if (!process.env.TFA_ENABLED) { return false; }
    if (target.u2f_enabled && target.mfaStorage.deviceAuthentication) {
      return target.mfaStorage.deviceAuthentication.userPresence === 1;
    }
    return false;
  } catch (err) {
    reportHandler({ reporter: res.json, err });
  }
};

module.exports = {
  getU2FState,
  resetU2F,
  firstRegisterStage,
  secondRegisterStage,
  firstLoginStage,
  secondLoginStage,
};
