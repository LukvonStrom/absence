const { sendTemplateEmail, reportHandler } = require('../util');

function sendUserTemporaryPassword(data) {

}

/**
 *
 * @param data {resetLink: "https://...", school: {title:"Gymnasium", short: "Rutesheim", logo: 'https://intranet.planschule-it.de/GymRut/Format/GymRut.jpg'}, user: {email: "", name: ""}}
 */
function resetUserPassword(data) {
  sendTemplateEmail({
    recipient: data.user.email,
    subject: 'Passwortänderung',
    templateName: 'password-reset',
    additional: {
      name: data.user.name,
      link: data,
      resetLink,
      year: (new Date()).getFullYear(),
    },
  });
}

function enableTwoFactorAuth(data) {

}

function disableTwoFactorAuth(data) {

}
// eslint-disable-next-line global-require
module.exports = () => require('burns').registerEvents({
  sendUserTemporaryPassword, resetUserPassword, enableTwoFactorAuth, disableTwoFactorAuth,
});
