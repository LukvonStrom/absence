const normalizePort = require('./normalizePort');
const reportHandler = require('./reportHandler');
const sendTemplateEmail = require('./sendTemplateEmail');
const exposeSchoolProperties = require('./exposeSchoolProperties');

module.exports = {
  normalizePort,
  reportHandler,
  sendTemplateEmail,
  exposeSchoolProperties,
};
