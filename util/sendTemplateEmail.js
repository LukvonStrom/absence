const sendMail = require('../controller/mail/TransportController');
const genericTemplator = require('../templates/genericTemplator');


function sendTemplateEmail({
  recipient, subject, templateName, attachments = [], additional = {},
}) {
  /*

   */
  genericTemplator(templateName, additional)
    .then(html => sendMail({
      recipient: recipient.email,
      subject,
      attachments,
      html,

    }));
}

module.exports = sendTemplateEmail;
