const fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);

module.exports = async () => {
  const schoolProperties = {
    schooltitle: process.env.SCHOOL_TITLE,
    short: process.env.SCHOOL_SHORT,
  };
  await writeFile('./public/src/schoolProperties.json', JSON.stringify(schoolProperties));
};
