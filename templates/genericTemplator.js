/* eslint-disable no-param-reassign */
const mjml = require('mjml');

const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);

const fillTemplate = function (templateString, templateVars) {
  let parsed = templateString;
  Object.keys(templateVars).forEach((key) => {
    const value = templateVars[key];
    parsed = parsed.replace(`\${${key}}`, value);
  });
  return parsed;
};


/**
 *
 * @param name TemplateName
 * @param info All Inputs
 * @returns {Promise<String>}
 */
module.exports = async (name, info) => {
  const templateFile = await readFile(`./${name}.xml`);
  const templateString = templateFile.toString();
  info.schoolpre = `${process.env.SCHOOL_TITLE}`;
  info.schoolmain = `${process.env.SCHOOL_SHORT}`;
  info.logo = `${process.env.SCHOOL_LOGO}`;
  const preparedTemplate = fillTemplate(templateString, info);
  return mjml(preparedTemplate, { keepComments: false, minify: true }).html;
};

