const express = require('express');
const request = require('request');

const Router = express.Router();

const SubstitutionController = require('../controller/substitution/controller');
const RenewController = require('../controller/authentication/RenewController');
const LoginController = require('../controller/authentication/LoginController');
const DataExportController = require('../controller/privacy/DataExport');
const { requestPassReset, finishPassReset } = require('../controller/authentication/PasswordLostController');
const {
  firstRegisterStage,
  secondRegisterStage,
  firstLoginStage,
  secondLoginStage,
} = require('../controller/authentication/2fa/U2FController');

const { middleware } = require('../middleware/AuthChecker');
const Limiter = require('../middleware/Limiter');

Router.post('/login', LoginController);
Router.get('/logo', (req, res) => request.get(process.env.SCHOOL_LOGO).pipe(res));

Router.post('/beginReset', requestPassReset);
Router.post('/finishReset', finishPassReset);

Router.get('/u2f/register', firstRegisterStage);
Router.post('/u2f/register', secondRegisterStage);

Router.use(middleware);
Router.use(Limiter);

Router.post('/api/substitution', SubstitutionController);
Router.get('/api/dataExport', DataExportController);

// [(req, res, next) => req.permission ="", permissionsMiddleware]
// or does [req.permission = "", permissionMiddleware] work?

// Router.post('/api/auth', LoginController);
Router.post('/api/auth/renew', RenewController);

Router.get('/authorized', (req, res) => res.json({ auth: true }));

Router.get('/u2f/auth', firstLoginStage);
Router.post('/u2f/auth', secondLoginStage);

module.exports = Router;

